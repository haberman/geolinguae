#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os
import re
import json
import argparse
import logging
import urllib

from datetime import datetime
from slugify import slugify

import translator
import geocoder
import private


def validate_langs(arg, source, translatable, countries):
    """
    Validates the `lang` arguments against translatable languages.

    Arguments
    arg          -- the argument value to validate
    source       -- the source language that is implicitely included before subsequent filtering
    translatable -- the list of languages for which we can target a translation
    countries    -- a list of countries to filter out eventual continents

    Returns a list of valid languages.
    """

    if not arg:
        return []
    translatables = [t['language'] for t in translatable]
    languages = set([source])

    # validates continents
    for c in countries:
        if c['continent'] not in re.findall(r'([A-Z]{2})', arg):
            continue

        for l in c['languages']:  # benefits of set: we don't need to check for uniqueness
            languages.add(l)

    # validates langs
    for f in re.findall(r'(-|\+)?([a-z]{2})', arg):
        if f[1] not in translatables:
            continue

        if f[0] == '-':
            languages.remove(f[1])
        else:
            languages.add(f[1])

    return languages


def validate_translatable(languages, translatable):
    """Returns a list of translatable languages from of a comma separated string of iso codes."""

    codes = languages.split(',')
    translatables = [t['language'] for t in translatable]

    res = []
    i = 0
    for code in codes:
        if len(code) < 5 and i > 0:
            continue
        if code[:2] not in translatables:
            continue

        i = i + 1
        res.append(code[:2])

    return res


def get_translations(query, source, translations):
    """Returns a list of translations a given query is associated with."""

    for sentences in translations:
        for lang in sentences:
            if lang == source and sentences[source] == query:
                return sentences

    return None


def get_translation(query, source, target, translations):
    """Returns the value of a translation matching a query in a given target language."""
    translations = get_translations(query, source, translations)

    try:
        return translations[target]
    except KeyError:
        return None


def main(args):
    cache_table = {
        'countries':      # list of geonamed countries (with names localizaed)
        {'file': 'cache/countries_{}.json'.format(args.source),
         'data': []},
        'translatable':   # iso codes for targetting translations
        {'file': 'cache/translatable_{}.json'.format(args.source),
         'data': []},
        'translations':   # list of previous translations' queries
        {'file': 'cache/translations.json',
         'data': []},
        'features': {}    # list of geocoded features for the query
    }

    if args.verbose:
        logging.info('Loading cache...')

    countries_file = cache_table['countries']['file']
    translatable_file = cache_table['translatable']['file']
    translations_file = cache_table['translations']['file']

    if not os.path.isdir('cache'):
        os.mkdir('cache')
    else:
        if os.path.isfile(countries_file):
            cache_table['countries']['data'] = json.load(open(countries_file))
        if os.path.isfile(translatable_file):
            cache_table['translatable']['data'] = json.load(open(translatable_file))
        if os.path.isfile(translations_file):
            cache_table['translations']['data'] = json.load(open(translations_file))

    # Gets countries of the world whose official language is translatable (args.source localized)
    if not cache_table['countries']['data']:
        if args.verbose:
            logging.info('No cache found for countries, querying Geonames...')

        countries = []
        for country in geocoder.country_info(private.GEONAMES_USERNAME, args.source):
            languages = validate_translatable(country['languages'],
                                              cache_table['translatable']['data'])

            if not languages:
                continue

            country['languages'] = languages
            countries.append(country)

        cache_table['countries']['data'] = countries

        with open(countries_file, 'w+') as cf:
            json.dump(cache_table['countries']['data'], cf)
            cf.close()

    # Gets Google supported translations (args.source localized)
    if not cache_table['translatable']['data']:
        if args.verbose:
            logging.info('No cache found for supported languages, querying Google...')

        try:
            translatable = translator.supported_languages(args.source)
        except urllib.error.HTTPError:
            logging.error('%s source language is not translatable.', args.source)
            exit(0)

        cache_table['translatable']['data'] = json.loads(translatable)['data']['languages']

        with open(translatable_file, 'w+') as tf:
            json.dump(cache_table['translatable']['data'], tf)
            tf.close()

    # We have translatable and countries to validate langs' filter against
    lang_filter = validate_langs(args.lang, args.source,
                                 cache_table['translatable']['data'],
                                 cache_table['countries']['data'])

    query_translations = get_translations(args.query, args.source,
                                          cache_table['translations']['data'])
    translation_cache_changed = False

    if not query_translations:
        query_translations = {args.source: args.query}
        translation_cache_changed = True

    # Gets countries whose one of its official language is in the `lang_filter` list
    # Also translates the query into that language if not present in cache
    langland = []
    for lang in lang_filter:
        try:
            query_translations[lang]
        except KeyError:
            if args.verbose:
                logging.info('Translating %s into %s...', args.query, lang)

            t = translator.translate(args.query, args.source, lang)
            translations = json.loads(t)['data']['translations']

            if len(translations) == 0:
                if args.verbose:
                    logging.error('No translation available for %s', lang['name'])
                continue

            query_translations[lang] = translations[0]['translatedText']
            translation_cache_changed = True

        for c in cache_table['countries']['data']:
            if lang in c['languages']:
                langland.append(c)

    if translation_cache_changed:
        cache_table['translations']['data'].append(query_translations)

    with open(translations_file, 'w+') as tf:
        json.dump(cache_table['translations']['data'], tf)
        tf.close()

    langland = list({v['id']: v for v in langland}.values())  # ensure uniqueness

    # Finally automates the geojson creation with geocoded results matching query and filters
    # spinner = Spinner('{Translating} {}... '.format(args.query))
    if args.countries:
        filtered_countries = []
        countries_filter = args.countries.split(',')
        for country in langland:
            _languages = set(country['languages']) - lang_filter

            try:
                if country['code'] in countries_filter and len(_languages) < len(lang_filter):
                    filtered_countries.append(country)
            except KeyError:
                continue

        langland = filtered_countries

    for country in langland:
        langs = country['languages']

        if args.verbose:
            logging.info('Geocoding query within %s', country['name'])

        for lang in langs:
            if lang not in lang_filter:
                continue

            geoquery = get_translation(args.query, args.source, lang,
                                       cache_table['translations']['data'])

            if args.verbose:
                print('    @{}: {}'.format(lang, geoquery))

            features_file = 'cache/{}_{}-{}.json'.format(slugify(geoquery), country['id'], lang)

            try:
                features = geocoder.features_in_country(geoquery, country['code'], lang)
            except KeyError:
                features = geocoder.features_in_bbox(geoquery, country['bbox'], lang)

            if not features:
                logging.warning('No features found for query: %s in %s',
                                geoquery, country['country_label'])
                continue

            raw_features = json.loads(features)
            cache_table['features'][features_file] = geocoder.features_to_geojson(raw_features)

            with open(features_file, 'w+') as ff:
                json.dump(cache_table['features'][features_file], ff)
                ff.close()

    if args.pack:
        pack_file = 'cache/{}_{}.json'.format(slugify(args.query),
                                              datetime.now().strftime('%Y-%m-%d_%H:%M:%S'))

        with open(pack_file, 'w+') as pf:
            features = []

            for k in cache_table['features']:
                for feature in cache_table['features'][k]['features']:
                    features.append(feature)

            if args.verbose:
                logging.info('Packing %d results into a single file', len(features))

            json.dump({'type': 'FeatureCollection', 'features': features}, pf)
            pf.close()


if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s: %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser(prog='./geolinguae.py',
                                     description='Automates the geocoding of a given query in different langs and countries.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-v', '--verbose', action='store_true',
                        help='increase output verbosity')
    parser.add_argument('-p', '--pack', action='store_true',
                        help='packs all features into a single geojson')
    parser.add_argument('-s', '--source', default='fr',
                        help='sets the language code of the query, the results\' localization and initial countries where to perform geocoding')
    parser.add_argument('-l', '--lang', default=None,
                        help='''a comma separated list of values to extend the query in coutries where an other lang is spoken.
                                When following a continent (allcaps) and preceeding a lang (smallcaps): `-` means exclude''')
    parser.add_argument('-c', '--countries', default=None,
                        help='filters results within a comma separated list of countries\' iso codes.')
    parser.add_argument('-t', '--type', default=None,
                        help='a comma separated list of values to filter results of a certain OSM type')
    parser.add_argument('query', help='the query to geocode')

    try:
        main(parser.parse_args())
    except IOError as msg:
        parser.error(str(msg))
