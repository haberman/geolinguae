import urllib.request as ureq
import urllib.parse as uparse

import private

API_URL = 'https://translation.googleapis.com/language/translate/v2'


def supported_languages(target):
    """Gets the supported translations for a given target language."""

    url = '{}/languages/?key={}&target={}'.format(API_URL, private.GOOGLE_API_KEY, target)
    
    req = ureq.urlopen(url)
    res = req.read()
    req.close()

    return res


def translate(query, source, target):
    """
    Posts a Google translation

    Arguments
    query  -- the string to translate
    source -- the query language
    target -- the translation language
    """

    data = {'key': private.GOOGLE_API_KEY, 'format': 'text',
            'q': query, 'target': target, 'source': source}

    req = ureq.urlopen(API_URL, data=uparse.urlencode(data).encode('utf-8'))
    res = req.read()
    req.close()

    return res
